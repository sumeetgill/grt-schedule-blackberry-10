# README #


### What is this repository for? ###

* This was a project I created during school. It used the Google Transit data in Waterloo to help users find the schedule at their bus stop. I used a local database to store all the information instead of querying an online database, due to reception coverage on campus being terrible.
* Version 1.0


### How do I get set up? ###

#### Summary of set up ####

* Download the files and run it in the BB10 ripple simulator. You can also package it and sideload it onto a BB10 phone.

#### Configuration ####

* If you wanted to use it today, you would need to load new SQL files with updated transit times.

#### Dependencies ####

* Completely dependent on GTFS data provided by the city via Google.

####Deployment instructions####

* Check configuration above.



### Contribution guidelines ###

I don't have guidelines because I don't really expect anyone to contribute to this. It would probably be better to use an off-device data source and write it natively.

### Who do I talk to? ###

* Me! Sumeetgill