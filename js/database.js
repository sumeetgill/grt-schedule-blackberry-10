// create database if does not exist
initializeDatabase = function(){

	html5sql.openDatabase(
	"com.pearprogramming.grtschedule.db",
	"GRT schedule ",
	35*1024*1024); // 35 mb database. Leaving room for improvement
	
	
	// creates the route table
	$.get('GRT-stop_times.sql',function(sql){
		html5sql.process(sql,
		function(){	
			//blackberry.ui.toast.show("Stop Times Table Created", options);
			localStorage.setItem('StopTimeTable', "Created");
			bb.pushScreen('home.htm', 'home');
		},
		function(error, failingQuery){
			//document.getElementById('output').innerHTML = "Error: "+ error.message + " when processing " + statement;
		}
		);	
	});
	
	// creates teh stops table
	$.get('GRT-stops.sql',function(sql){
		html5sql.process(sql,
		function(){	
			//blackberry.ui.toast.show("Stop Table Created", options);
			localStorage.setItem('StopsTable', "Created");
		},
		function(error, failingQuery){
			//document.getElementById('output').innerHTML = "Error: "+ error.message + " when processing " + statement;
		}
		);	
	});


	// creates teh stops table
	$.get('GRT-routes.sql',function(sql){
		html5sql.process(sql,
		function(){	
			//blackberry.ui.toast.show("Routes Table Created", options);
			localStorage.setItem('RoutesTable', "Created");
		},
		function(error, failingQuery){
			//document.getElementById('output').innerHTML = "Error: "+ error.message + " when processing " + statement;
		}
		);	
	});


	html5sql.process('CREATE TABLE IF NOT EXISTS favourites (favourite_id INTEGER PRIMARY KEY, stop_id INTEGER, route_id INTEGER);',
		function(){	
			//blackberry.ui.toast.show("FavouriteTable created" , options);
			localStorage.setItem('FavouriteTable', "Created")
		},
		function(error, failingQuery){
			//document.getElementById('error').innerHTML = "Error: "+ error.message + " when processing " + failingQuery;
		}
	);	
} // initialize database