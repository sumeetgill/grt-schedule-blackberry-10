	// spits out an error message
	onError = function(tx, error) {
		alert(error.message);
	}

	// this is the function that searches the SQL and actually spits out the records
	displayRecords = function(userStopID, userRouteID) {	
		//blackberry.ui.toast.show("Searching", options);	
		// declare variables
		var favourite = document.getElementById('favourite');
		//var favouriteButton = document.getElementById('favouriteButton').show();
		var valid = true;
		var searchTimes = true;
		var button = document.createElement('div');

				// Added if statements so we can try and have the same function for routes and favourites
		if (arguments.length == 0){
			favourite.innerHTML = "";
			var userStopID = document.getElementById("userStopID").value;
			var userRouteID = document.getElementById("userRouteID").value;
					
			// if user doesnt enter a route, it adjusts favourite button code
			if (userRouteID == "" && userStopID != ""){
				button.setAttribute('data-bb-type','button');
				button.setAttribute('onclick', 'addFavourite(' + userStopID + ')');
				button.innerHTML = "Add to Favourites"; // inner text
				button = bb.button.style(button); // Apply our styling
				favourite.appendChild(button);
				bb.refresh();
			}else if (userRouteID != "" && userStopID != ""){
				button.setAttribute('data-bb-type','button');
				button.setAttribute('onclick', 'addFavourite(' + userStopID + ',' + userRouteID + ')');
				button.innerHTML = "Add to Favourites";
				button = bb.button.style(button);
				favourite.appendChild(button);
				bb.refresh();
			} // if statement
		}else if (arguments.length == 2){ // for when user searches route only and then selects something
			if(localStorage.getItem('favStopID') == "" && localStorage.getItem('favRouteID') == ""){	
				favourite.innerHTML = '<div align="center"><div data-bb-type="button" id="favouriteButton" onclick="addFavourite(' + userStopID + ',' + userRouteID + ')">Add to Favourites</div></div>';
				favouriteButton = bb.button.style(favouriteButton);
				bb.refresh();
			}
			
			// if one argument comes through undefined or null, blanks it out so select statement can set up properly
			if (userRouteID == "undefined" || userRouteID == "null"){
				userRouteID = "";
			}
			// if one argument comes through undefined or null, blanks it out so select statement can set up properly
			if (userStopID == "undefined" || userStopID == "null"){
				userStopID = "";
			}
		} // if argument length
		
		// opens database for querying
		var db = openDatabase("com.pearprogramming.grtschedule.db", "", "GRT schedule", 35*1024*1024); // 15 mb database. Look to reduce in the future
		
		if(userRouteID == "" && userStopID == ""){
			searchTimes = false;
		}
		
		// For route times, if user did not specify bus, then all buses are queried
		// for when user has favourite without stopID
		if (userRouteID == "" && userStopID != ""){
			var selectStatement = "SELECT route_id, arrival_time, stop_id  FROM stop_times WHERE stop_id=" + userStopID + " ORDER BY arrival_time asc";
		}else if (userRouteID != "" && userStopID != ""){ // alternative query for when user enters a route_id
			var selectStatement = "SELECT route_id, arrival_time, stop_id FROM stop_times WHERE stop_id='" + userStopID + "' AND route_id='" + userRouteID + "' ORDER BY arrival_time asc";
		}else if (userRouteID != "" && userStopID == ""){
			var selectStatement = "SELECT DISTINCT stops.stop_id, stops.stop_name, stop_times.route_id " 
								+ "FROM stops " 
								+ "INNER JOIN stop_times ON stop_times.stop_id = stops.stop_id "
								+ "WHERE route_id=" + userRouteID
								+ " ORDER BY stop_name asc";
			searchTimes = false;
			// query for stop names when given route ID
			db.transaction(function (transaction) {
				transaction.executeSql(selectStatement,
					[],
					function (transaction, results) { onGetAllRoutesForStopsSuccess(results) },
					onError);
			});
		} //if (userRouteID != "" && userStopID == "")
		
		// this if is so it doesn't unecessarily trigger every time
		if (searchTimes){
			// For stop name
			var stopSelectStatement = "SELECT stop_name FROM stops WHERE stop_id=" + userStopID;
			// query for route times
			db.transaction(function (transaction) {
				transaction.executeSql(selectStatement,
					[],
					function (transaction, results) { onGetAllRoutesSuccess(results) },
					onError);
			});
			// query database for stop name
			db.transaction(function (transaction) {
				transaction.executeSql(stopSelectStatement,
					[],
					function (transaction, results) { onGetAllStopsSuccess(results) },
					onError);
			});
		} //if (searchTimes)
	} // displayrecords bracket
	
	// output for the stop TIMES
	onGetAllRoutesSuccess = function(results) { 
		window.location.hash = '#stop'; // sets focus lower so textboxes are hidden
		//document.getElementById("stop").focus();
		// clear out data or else append will keep sticking it at the bottom
		document.getElementById('dataList').innerHTML = ""; 
		document.getElementById('stop').innerHTML = "";
		document.getElementById('error').innerHTML = "";

		var error = document.getElementById("error");
		var aRow = null;
		
		// if no records are found, tells user
		if (results.rows.length == 0) {
			error.innerHTML = '<h4>No records found. Please ensure that correct information was entered.</h4>';
			document.getElementById('favourite').innerHTML = "";
		}
		
		// read each of the rows from the results
		for (var i = 0; i < results.rows.length; i++) {
			aRow = results.rows.item(i);

			// outputs it neatly to a listview
			listItem = document.createElement('div');
			listItem.setAttribute('id', 'menuItem');
			listItem.setAttribute('data-bb-type', 'item');
			listItem.setAttribute('data-bb-title', aRow['arrival_time']);
			listItem.innerHTML = 'Route ID: ' + aRow['route_id'];
			
			dataList.appendItem(listItem); // Append the item
		}
		// removes the local storage items so that they do no constantly cause queries when routes is loaded
		localStorage.setItem('favStopID', "");
		localStorage.setItem('favRouteID', "");
	} // onGetAllRoutesSuccess bracket
	
	// output for the stop NAME
	onGetAllStopsSuccess = function(results){
		var stop = document.getElementById("stop");
		var aRow = null; // initialise a string to hold the html line items

		// tells user that no results available
		if (results.rows.length == 0) {
			stop.innerHTML = '<h4>Stop name not found.</h4>';
		}
		
		// read each of the rows from the results
		for (var i = 0; i < results.rows.length; i++) {
			aRow = results.rows.item(i);
			// outputs it neatly to a div	
			stop.innerHTML = '<h4>'+ aRow['stop_name']+ '</h4>';
			bb.refresh();
		}
	} //onGetAllStopsSuccess bracket
	
	// output for the stop NAME
	onGetAllRoutesForStopsSuccess = function(results){ 
		window.location.hash = '#stop'; // sets focus lower so textboxes are hidden
		// clear out data or else append will keep sticking it at the bottom
		document.getElementById('dataList').innerHTML = ""; 
		document.getElementById('stop').innerHTML = "";
		document.getElementById('error').innerHTML = "";
		document.getElementById('favourite').innerHTML = "";
	
		var error = document.getElementById("error");
		// initialise a string to hold the html line items
		var aRow = null;
	
		// if no records are found, tells user
		if (results.rows.length == 0) {
			error.innerHTML = '<h4>No records found. Please ensure that correct information was entered.</h4>';
		}
		// read each of the rows from the results
		for (var i = 0; i < results.rows.length; i++) {
			aRow = results.rows.item(i);
			
			listItem = document.createElement('div');
			listItem.setAttribute('id', 'menuItem');
			listItem.setAttribute('data-bb-type', 'item');
			listItem.setAttribute('data-bb-title', aRow['stop_name']);
			listItem.setAttribute('onclick', 'displayRecords(' + aRow['stop_id'] + ',' + aRow['route_id'] + ')');
			listItem.innerHTML = 'Stop ID: ' + aRow['stop_id'];
			
			dataList.appendItem(listItem); // Append the item
		}
	} //onGetAllStopsSuccess bracket
	