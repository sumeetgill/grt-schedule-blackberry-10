	function getPosition()
	{
	   //First test to see that the browser supports the Geolocation API
	   if (navigator.geolocation !== null)
	   {
	   	  alert("Getting position");
	      var options;
	      navigator.geolocation.getCurrentPosition(geolocationSuccess, geolocationError, options);
	   }
	   else {
	      alert("HTML5 geolocation is not supported.");
	   }
	}

	function geolocationError(error)
	{
	   alert("An unexpected error occurred [" + error.code + "]: " + error.message);
	}

	function geolocationSuccess(position)
	{
		var coordinates = position.coords;

		//Retrieve geographic information about the GPS fix:
		var current_lat = coordinates.latitude;
		var current_lon = coordinates.longitude;

		localStorage.setItem("lat", current_lat);
		localStorage.setItem("lon", current_lon);

		var db = openDatabase("com.pearprogramming.grtschedule.db", "", "GRT schedule", 35*1024*1024);

		var stopSelectStatement = "SELECT stop_name, stop_lon, stop_lat, stop_id FROM stops ORDER BY stop_id;";
		// query database for stop name
		db.transaction(function (transaction) {
				transaction.executeSql(stopSelectStatement,
					[],
					function (transaction, results) { onGetAllStopsSuccess(results) },
					onError);
		});
	}

		// output for the stop NAME
	onGetAllStopsSuccess = function(results){
		var stop = document.getElementById("stop");
		var aRow = null; // initialise a string to hold the html line items

		// tells user that no results available
		if (results.rows.length == 0) {
			stop.innerHTML = '<h4>Stop name not found.</h4>';
		}
		
		// read each of the rows from the results
		for (var i = 0; i < results.rows.length; i++) {
			aRow = results.rows.item(i);

			var distance = distanceBetweenPoints(localStorage['lat'], localStorage['lon'], aRow['stop_lat'], aRow['stop_lon']);

			if (distance < 500) {
				listItem = document.createElement('div');
				listItem.setAttribute('id', 'menuItem');
				listItem.setAttribute('data-bb-type', 'item');
				listItem.setAttribute('data-bb-title', aRow['stop_name']);
				listItem.setAttribute('onclick', 'showFavRecords(' + aRow['stop_id'] + ')');
				listItem.innerHTML = 'Distance: ' +  distance + "m";
				
				dataList.appendItem(listItem); // Append the item
			}

		}
	} //onGetAllStopsSuccess bracket

	function distanceBetweenPoints(current_lat, current_lon, target_lat, target_lon)
	{
		var distance = 0;
		try
		{
			//Radius of the earth in meters:
			var earth_radius = 6378137;
			
			//Calculate the distance, in radians, between each of the points of latitude/longitude:
			var distance_lat = (target_lat - current_lat) * Math.PI / 180;
			var distance_lon = (target_lon - current_lon) * Math.PI / 180;

			//Using the haversine formula, calculate the distance between two points (current & target GPS coordinates) on a sphere (earth):
			//More info: http://www.movable-type.co.uk/scripts/latlong.html
			var a = Math.pow(Math.sin(distance_lat / 2), 2) + (Math.cos(current_lat * Math.PI / 180) * Math.cos(target_lat * Math.PI / 180) * Math.pow(Math.sin(distance_lon / 2), 2));
			var b = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
			distance = Math.floor(earth_radius * b);
		} 
		catch (e) {
			errorMessage("exception (distanceBetweenPoints): " + e);
		}
		return distance;
	}