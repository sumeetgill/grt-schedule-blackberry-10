/**
 *  openlayers
 */

// initialize the map
function initOpenLayersMaps() {
  openLayersMap = new OpenLayers.Map('map_canvas');
  layer = new OpenLayers.Layer.OSM("Simple OSM Map");
  openLayersMap.addLayer(layer);
  //openLayersMap.setCenter(
  //   new OpenLayers.LonLat(myLong, myLat).transform(
  //   new OpenLayers.Projection("EPSG:4326"), openLayersMap.getProjectionObject()
  // ), 14);
  searchStopLocations();
}

// // search for nearby places
// function initOpenLayersPlaces() {
//   searchForPlaces(openLayersPlacesCallback);
// }

// // search callback
// function openLayersPlacesCallback(results) {
//   if(results) {
//     openLayersOverlay = new OpenLayers.Layer.Vector('Overlay', {
//       styleMap: new OpenLayers.StyleMap({
//         externalGraphic: 'http://www.openlayers.org/dev/img/marker.png',
//         graphicWidth: 20,
//         graphicHeight: 24,
//         graphicYOffset: -24,
//         title: '${tooltip}'
//       })
//     });
//     openLayersMap.addLayer(openLayersOverlay);
//     createOpenLayersMarker(results);
//   }
// }

// create marker / push-pin
function createOpenLayersMarker(results) {

    var aRow = null;
    for(var i = 0; i < results.length; i++) {
      aRow = results.rows.item(i);

      //var lat = results[i].geometry.location.Ya;
      //var lon = results[i].geometry.location.Za;
      var lat = aRow['stop_lat'];
      var lon = aRow['stop_lon'];
      alert(lat + " " + lon);
      var myLocation = new OpenLayers.Geometry.Point(lon, lat).transform('EPSG:4326', 'EPSG:3857');
      openLayersOverlay.addFeatures([
      new OpenLayers.Feature.Vector(myLocation)]);
    }
}


searchStopLocations = function(){
  var db = openDatabase("com.pearprogramming.grtschedule.db", "", "GRT schedule", 35*1024*1024);
  db.transaction(function (transaction) {
      transaction.executeSql("SELECT stop_lat, stop_lon from stops",
        [],
        function (transaction, results) { createOpenLayersMarker(results) },
        onError);
  });

}

  onError = function(tx, error) {
    alert(error.message);
  }


