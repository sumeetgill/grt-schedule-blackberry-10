// tells user about favourite being added
displayAddCompleted = function() {
	alert("Favourite Added.");
	//blackberry.ui.toast.show("Favourite Added.", options);	
}

// awesome error handler
errorHandler = function(){
	//alert("Uh oh hot dog.");
	document.getElementById("error").innerHTML = "An Error has occured.";
}

addFavourite = function(stop_id, route_id) {
	var sqlString = "INSERT INTO favourites (stop_id, route_id) VALUES (?, ?);";
	
	// opens database for querying
	var db = openDatabase("com.pearprogramming.grtschedule.db", "", "GRT schedule", 35*1024*1024);
	db.transaction(function (transaction) {
		transaction.executeSql(
			sqlString,
			[stop_id, route_id],
			displayAddCompleted,
			errorHandler
		);
	}); // end transaction
} // add favourite

// function that does the actual displaying of information
 onGetAllFavouritesSuccess = function(results) { 
	var error = document.getElementById("error");
	error.innerHTML = "";
    // initialise a string to hold the html line items
    var aRow = null;
	
	// tells user no records found
    if (results.rows.length == 0) {
        error.innerHTML =  '<ul align="left">'
				+ '<li>To add favourites, go to routes and enter the stop/route that you want to save. </li>'
				+ '<li>Click "Add to favourites" when prompted to.</li>'
				+'</ul>';
    } //if
	
	var listItem, container, dataList = document.getElementById('favDataList');
	dataList.innerHTML = "";
    // read each of the rows from the results
    for (var i = 0; i < results.rows.length; i++) {
        //  output += JSON.stringify(results.rows.item(i));
        aRow = results.rows.item(i);
		
		// Create our list item
		listItem = document.createElement('div');
		listItem.setAttribute('id', 'menuItem');
		listItem.setAttribute('data-bb-type', 'item');
		listItem.setAttribute('data-bb-title', aRow['stop_name']);
		listItem.setAttribute('onbtnclick', 'deleteFavourite(' + aRow['favourite_id'] + ')');
		listItem.setAttribute('onclick', 'showFavRecords(' + aRow['stop_id'] + ',' + aRow['route_id'] + ')');
		listItem.innerHTML = 'Stop ID: ' + aRow['stop_id'] + ' |  Route ID: '+ aRow['route_id'];
		// Append the item
		dataList.appendItem(listItem);
    } // for
} // onGetAllFavouritesSuccess

// function that does the querying of database
getAllFavourites  = function() {
	var selectStatement = "SELECT favourites.favourite_id, favourites.stop_id, favourites.route_id, stops.stop_name FROM favourites, stops WHERE favourites.stop_id = stops.stop_id ORDER BY favourite_id asc";

	// opens database for querying
	var db = openDatabase("com.pearprogramming.grtschedule.db", "", "GRT schedule", 35*1024*1024);
    db.transaction(function (transaction) {
        transaction.executeSql(selectStatement,
				[],
                function (transaction, results) { onGetAllFavouritesSuccess(results) },
                errorHandler);
    });
} // getAll

deleteFavourite = function(favourite_id) {
	var confirmation = confirm("Are you sure you want to delete that?")
	if (confirmation){
		var sqlString = "DELETE FROM favourites WHERE favourite_id = " + favourite_id; // user can have same stop favourited with multiple buses. Needs to delete by key
		// confirm delete
		// opens database for querying

		var db = openDatabase("com.pearprogramming.grtschedule.db", "", "GRT schedule", 35*1024*1024);
		db.transaction(function (transaction) {
				transaction.executeSql(
						sqlString,
						[],
						function () {
							getAll();
						},
						errorHandler);
		});
	}
} // deleteFavourite

// this method sets values into local storage so that they can be used on routes page. ONLOAD too unreliable to use when passing arguments.
showFavRecords = function(StopID, RouteID){
		localStorage.setItem('favStopID', StopID);
		localStorage.setItem('favRouteID', RouteID);
		bb.pushScreen('routes.htm', 'routesFav');
}