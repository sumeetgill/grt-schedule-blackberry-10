displayInfo = function(){
	var output = document.getElementById("output");
	var selected = document.getElementById("selections").value;
	var appname = blackberry.app.name;
	var appversion = blackberry.app.version;
	var appauthor = blackberry.app.author;
	
	output.innerHTML = "";
	
	if(selected == "App Info"){
		output.innerHTML += "<h4>Upcoming features:</h4>"
		+ "<ul align='left'>"
			+ "<li>Comprehensive route information</li>"
			+ "<li>Performance improvements</li>"
		+ "</ul><br />"
		+ "<p>All data provided and owned by the "
			+ "<a href='http://www.grt.ca/en/doingBusiness/supportingsoftwareapplications.asp?_mid_=18101' target='_blank'>Region of Waterloo.</a>"
		+ "</p><br />";
		output.innerHTML += appname + ": " + appversion + "<br>"; 
	}else if(selected == "About Us"){
		output.innerHTML += "Developed By:<ul align='left'><li>Sumeet Gill</li></ul>"; 
	}else if(selected == "Contact Us"){
		output.innerHTML = "Please feel free to send an email with any questions, features you would like to see and/or bug submissions. <br /><br /><br />" + 
		"<a href='mailto:contact@pearprogramming.biz'>contact@pearprogramming.biz</a>";
	}
}
displayHelp = function(){
	var output = document.getElementById("output");
	var selected = document.getElementById("selections").value;
	output.innerHTML = "";
	
	if(selected == "Tutorial"){
		output.innerHTML += "<h4>Maps</h4>"
		+ "<ul align='left'>"
			+ "<li>From home, select Maps. From this page, you will be able to find a specific bus stop.</li>"
			+ "<li>To Zoom in, pinch out on map</li>"
			+ "<li>To Zoom out, pinch in on map</li>"
			+ "<li>Click on the stop you are wanting. Here you will find the stop id</li>"
			+ "<li>Press back button</li>"
		+ "</ul><br />"
		+ "<h4>Routes</h4>"
		+ "<ul align='left'>"
			+ "<li>From home, select Routes. You will be able to find times for stops and routes</li>"
			+ "<li>You are able to enter in just the stop number if you're interested in searching every bus at that particular stop.</li>"
			+ "<li>Entering in only the bus number will output a list of every stop that the bus visits.</li>"
			+ "<li>Inputting both stop number and bus number will output only that specific bus number for that stop.</li>"
			+ "<li>After entering in your desired query, click the Search Routes button or press enter</li>"
			+ "<li>A list will appear with your desired results</li>"
			+ "<li>Click Add to Favourites button if you would like to remember this stop for later</li>"
		+ "</ul><br />"
		+ "<h4>Favourites</h4>"
		+ "<ul align='left'>"
			+ "<li>From home, select Favourites. You will be able to see the favourite routes and stops you have added.</li>"
			+ "<li>Press on the favourite route or stop you would like to see.</li>"
			+ "<li>The route times will be displayed</li>"
		+ "</ul><br />";
	}else if(selected == "Menu"){
		output.innerHTML += "On each page, you can swipe down from the top of the screen to show a menu that has a quick like to the home screen, the help screen, and the settings screen."; 
	}else if(selected == "Fares"){
		output.innerHTML = "Here you will be able to find a breakdown of all transit fares.";
	}else if(selected == "Other"){
		output.innerHTML += "<h4>Select an option from the drop down<br />About the App</h4>"
		+ "<ul align='left'>"
			+ "<li>Information about the app, including app version</li>"
		+ "</ul><br />"
		+ "<h4>About Us</h4>"
		+ "<ul align='left'>"
			+ "<li>Information about the developers</li>"
		+ "</ul><br />"
		+ "<h4>Contact Us</h4>"
		+ "<ul align='left'>"
			+ "<li>Email address and website of the developers</li>"
		+ "</ul><br />";
	}else if(selected == "Settings"){
		output.innerHTML += "<h4>Select an option from the drop down<br />About the App</h4>"
		+ "<ul align='left'>"
			+ "<li>If your database becomes corrupted, press the Reset Database button </li>"
			+ "<li>Once the button is pressed, on next load of the application, the database will reinstall itself</li>"
		+ "</ul><br />";
	}
}