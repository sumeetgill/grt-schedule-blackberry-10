DROP TABLE IF EXISTS favourites;
CREATE TABLE favourites (favourite_id INTEGER PRIMARY KEY, stop_id INTEGER, route_id INTEGER);